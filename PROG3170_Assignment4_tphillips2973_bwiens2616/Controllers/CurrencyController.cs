﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using PROG3170_Assignment4_tphillips2973_bwiens2616.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;

namespace PROG3170_Assignment4_tphillips2973_bwiens2616.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {
        #region Class Declarations
        private readonly CurrencyContext _context;

        public CurrencyController(CurrencyContext context)
        {
            _context = context;
        }
        
        //URL and Parameters for getting USD value
        private string currencyURL = @"https://api.exchangeratesapi.io/";
        private string currencyURLParameters = "latest?base=CAD&symbols=USD";

        //URL and Parameters for getting BitCoin value
        private string cryptocurrencyURL = @"https://api.coindesk.com/v1/bpi/currentprice/";
        private string cryptocurrencyURLParameters = @"USD.json";
        #endregion

        //Used to get either current USD dollar value based on baseCurrency entered by User
        //Or to get current BitCoin value
        public static string GetUpdatedCurrencyData(string URL, string parameters)
        {
            string result = "";
            var client = new RestClient(URL);
            var request = new RestRequest(parameters);
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            try
            {
                if (response.IsSuccessful || response.StatusCode == HttpStatusCode.OK)
                {
                    var content = JsonConvert.DeserializeObject<JToken>(response.Content);
                    dynamic api = JObject.Parse(content.ToString());
                    if (URL == "https://api.exchangeratesapi.io/")
                        result = (string)api.rates.USD.ToString();
                    if (URL == "https://api.coindesk.com/v1/bpi/currentprice/")
                        result = (string)api.bpi.USD.rate.Value.ToString();
                }
                else
                {
                    return response.Content.ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return result;
        }

        #region GET Methods
        // GET: api/currency/GetUpdatedCurrency?baseCurrency=CAD&cashValue=12000
        [HttpGet]
        [HttpGet("GetUpdatedCurrencyFromQuery")]
        public string[][] GetUpdatedCurrencyFromQuery([FromQuery]string baseCurrency, [FromQuery]double cashValue)
        {
            return GetUpdatedCurrency(baseCurrency, cashValue);
        }

        // GET: api/currency/GetUpdatedCurrencyFromBody/CAD/12000
        [HttpGet]
        [HttpGet("GetUpdatedCurrencyFromBody/{baseCurrency}/{cashValue}")]
        public string[][] GetUpdatedCurrencyFromBody(string baseCurrency, double cashValue)
        {
            return GetUpdatedCurrency(baseCurrency, cashValue);
        }

        public string[][] GetUpdatedCurrency(string baseCurrency, double cashValue)
        {
            try
            {
                //Get Value inputted in USD
                cashValue = Math.Round(cashValue, 2);
                currencyURLParameters = (@"latest?base=" + baseCurrency + @"&symbols=USD");
                double convertedToUSDValue = Math.Round(cashValue *
                    Convert.ToDouble(GetUpdatedCurrencyData(currencyURL, currencyURLParameters)), 2);

                //Get Current BitCoin Value and 
                double bitCoinTotalValue = Math.Round(Convert.ToDouble
                    (GetUpdatedCurrencyData(cryptocurrencyURL, cryptocurrencyURLParameters)), 2);
                double bitCoinEqual = Math.Round(convertedToUSDValue / bitCoinTotalValue, 2);

                //Return formatted response
                return new string[][] { new string[] { "Value in " + baseCurrency, cashValue.ToString() },
                new string[] { "Value in USD", convertedToUSDValue.ToString() },
                new string[] { "Value of Bitcoin", bitCoinTotalValue.ToString(),
                    "Number of Bitcoins you can purchase (Rounded)", Math.Floor(bitCoinEqual).ToString(),
                    "Amount of Bitcoins in value", bitCoinEqual.ToString() } };
            }
            catch (Exception ex)
            {
                if (ex.Message == "Input string was not in a correct format.")
                    return new string[][] { new string[] { "*ERROR* You entered a " +
                        "baseCurrency that is not allowed. Please see the list below." },
                        new string[] { "CAD", "HKD", "ISK", "PHP", "DKK", "HUF", "CZK", "AUD",
                            "RON", "SEK", "IDR", "INR", "BRL", "RUB", "HRK", "JPY", "THB", "CHF", "SGD",
                            "PLN", "BGN", "TRY", "CNY", "NOK", "NZD", "ZAR", "USD", "MXN", "ILS", "GBP",
                            "KRW", "MYR",} };
                if (ex.Message != "Input string was not in a correct format.")
                    return new string[][] { new string[] { "*ERROR*",
                        "An unknown error occurred, please double check your inputted variables and try again." } };
            }
            return new string[][] { new string[] { "*ERROR*",
                "An unknown error occurred, please double check your inputted variables and try again." } };
        }

        // GET: api/Currency/GetSpecificCurrency/5
        [HttpGet]
        [HttpGet("GetSpecificCurrency/{id}")]
        public async Task<ActionResult<Currency>> GetSpecificCurrency([FromBody]long id)
        {
            var currency = await _context.Currencies.FindAsync(id);

            if (currency == null)
            {
                return NotFound();
            }

            return currency;
        }

        // GET: api/Currency/GetAllPostedCurrencies
        [HttpGet]
        [HttpGet("GetAllPostedCurrencies")]
        public async Task<ActionResult<IEnumerable<Currency>>> GetSongs() => await _context.Currencies.ToListAsync();

        #endregion

        // POST: api/Currency
        [HttpPost]
        public async Task<ActionResult<Currency>> PostCurrencyInfo([FromForm] Currency currency)
        {
            currency.cashUSDValue = Math.Round(currency.cashValue *
                    Convert.ToDouble(GetUpdatedCurrencyData(currencyURL, currencyURLParameters)), 2); 
            currency.bitcoinUSDValue = Math.Round(Convert.ToDouble
                    (GetUpdatedCurrencyData(cryptocurrencyURL, cryptocurrencyURLParameters)), 2);
            currency.totalBitCoinValue = Math.Round(currency.cashUSDValue / currency.bitcoinUSDValue, 2);
            currency.totalBitCoinPurchasable = Math.Floor(currency.totalBitCoinValue);

            _context.Currencies.Add(currency);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSongs", new { currency.id }, currency);
        }

        // DELETE: api/Currency/5
        [Route("{id:long}")]
        [HttpDelete("{id}")]
        public async Task<ActionResult<Currency>> DeleteCurrencyInfo(long id)
        {
            var currency = await _context.Currencies.FindAsync(id);
            if (currency == null)
            {
                return NotFound();
            }

            _context.Currencies.Remove(currency);
            await _context.SaveChangesAsync();

            return currency;
        }

        private bool CurrencyIDExists(long id)
        {
            return _context.Currencies.Any(e => e.id == id);
        }
    }
}
