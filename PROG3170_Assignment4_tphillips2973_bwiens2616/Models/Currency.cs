﻿using System.ComponentModel.DataAnnotations;

namespace PROG3170_Assignment4_tphillips2973_bwiens2616.Models
{
    public class Currency
    {
        public long id { get; set; }

        [Required]
        public string baseCurrency { get; set; }
        [Required]
        public double cashValue { get; set; }
        public double cashUSDValue { get; set; }
        public double bitcoinUSDValue { get; set; }
        public double totalBitCoinPurchasable { get; set; }
        public double totalBitCoinValue { get; set; }
    }
}
