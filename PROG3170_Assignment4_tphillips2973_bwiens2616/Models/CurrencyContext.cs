﻿using Microsoft.EntityFrameworkCore;

namespace PROG3170_Assignment4_tphillips2973_bwiens2616.Models
{
    public class CurrencyContext : DbContext
    {
        public CurrencyContext(DbContextOptions<CurrencyContext> options)
               : base(options)
        {

        }

        public DbSet<Currency> Currencies { get; set; }
    }
}
